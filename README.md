To initialize environment:

```
fsetup Athena,22.0.49
```

To build (from folder above the rad_muon_secondaries git area):

```
mkdir build
cd build
cmake -DROOT_LCGROOT=$ROOTSYS -DCMAKE_MODULE_PATH=$G4INSTALL/cmake/modules -DCMAKE_INSTALL_PREFIX=../run ../rad_muon_secondaries
make install
```
