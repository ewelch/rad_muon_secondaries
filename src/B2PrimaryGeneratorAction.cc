//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file B2PrimaryGeneratorAction.cc
/// \brief Implementation of the B2PrimaryGeneratorAction class

#include "B2PrimaryGeneratorAction.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include <random>
 #include "Randomize.hh"
#include <cstdlib>
#include "csv.h"
#include <time.h>
#include "stdlib.h"
#include <fstream>
#include <iostream>


//#include <TRandom3.h>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B2PrimaryGeneratorAction::B2PrimaryGeneratorAction()
  : G4VUserPrimaryGeneratorAction(),muEnVec((0.,0.)),fEnergy(0.0),fxcoord(0.0),fycoord(0.0),fxcosDirec(0.0),fycosDirec(0.0),fage(0.0)
{

  srand(time(NULL));

  G4int nofParticles = 1;
  fParticleGun = new G4ParticleGun(nofParticles);

  // default particle kinematic
  //for just energies
  /*G4ParticleDefinition* particleDefinition 
    = G4ParticleTable::GetParticleTable()->FindParticle("mu-");

  fParticleGun->SetParticleDefinition(particleDefinition);
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1.));
  */


  // initialize the random number generator

double drand = rand()*1.0;
double drandmax = RAND_MAX*1.0;
double randomNum = drand/drandmax;


  //TRandom3 randGen;
  //randGen.SetSeed(std::time(0));

  // load the ROOT file for cdf
TFile* rootFile = TFile::Open("/afs/cern.ch/work/e/ewelch/public/rad_muon_study/rad_muon_study/secondary_event_generator/fluka_cdf.root", "READ");

TGraph *g;
  rootFile->GetObject("graph_cdf", g);

  // get the cdf from file
  //graph_cdf

  // Generate a random number between 0 and 1
  //double randomNum = randGen.Rndm();

  // get energy corresponding to the random CDF value
  double selectedEnergy = g->Eval(randomNum);

  
  rootFile->Close();

  //choose closest energy in fluka sample

  double closestEnergy = 0.0;
  double minDifference = std::numeric_limits<double>::max(); 
  std::string closestRow;
  std::ifstream inputFile("unit30_Nm");
  std::string line;
  while (std::getline(inputFile, line)) {
      std::istringstream iss(line);
      std::vector<std::string> tokens;
      std::string token;

      
      while (std::getline(iss, token, ' ')) {
          tokens.push_back(token);
      }

        // Extract the energy value from the 5th entry
        double energy = std::stod(tokens[4]);
        double xcoord =std::stod(tokens[6]);
        double ycoord =std::stod(tokens[7]);
        double xcosDirec =std::stod(tokens[8]);
        double ycosDirec =std::stod(tokens[9]);
        double age =std::stod(tokens[10]);
        // Calculate the absolute difference for choosing closest
        double difference = std::abs(energy - selectedEnergy );

        // Check if this is the closest energy so far
        if (difference < minDifference) {
            minDifference = difference;
            closestEnergy = energy;
            closestRow = line;
            fEnergy=energy;
            fxcoord=xcoord;
            fycoord=ycoord;
            fxcosDirec=xcosDirec;
            fycosDirec=ycosDirec;
            fage=age;
        }
      
  }

  inputFile.close();




  

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B2PrimaryGeneratorAction::~B2PrimaryGeneratorAction()
{
  delete fParticleGun;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void B2PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  // This function is called at the begining of event

  // In order to avoid dependence of PrimaryGeneratorAction
  // on DetectorConstruction class we get world volume
  // from G4LogicalVolumeStore.

  G4double worldZHalfLength = 0;
  G4LogicalVolume* worldLV
    = G4LogicalVolumeStore::GetInstance()->GetVolume("World");
  G4Box* worldBox = NULL;
  if ( worldLV ) worldBox = dynamic_cast<G4Box*>(worldLV->GetSolid());
  if ( worldBox ) worldZHalfLength = worldBox->GetZHalfLength();
  else  {
    G4cerr << "World volume of box not found." << G4endl;
    G4cerr << "Perhaps you have changed geometry." << G4endl;
    G4cerr << "The gun will be place in the center." << G4endl;
  }

 

  //for simple energy spectra
  /*io::CSVReader<2> in("rMuonMinusEnergiesAfterRock.csv");
   in.read_header(io::ignore_extra_column, "num", "energy");
   double num; double energy;
   while(in.read_row(num, energy)){
    
  muEnVec.push_back(std::make_pair(num,energy));
   }

   
double drand = rand()*1.0;
double drandmax = RAND_MAX*1.0;
double randnum = drand/drandmax;
 

G4double eFin = 0.0;

for (int i=0;i<(muEnVec.size()-2);i++)    
 {
       if((muEnVec[i].first< randnum) && (randnum < muEnVec[i+2].first))
 { 
eFin = muEnVec[i+1].second;
} 

 }
  */
//////////////////////////////////


//for full fluka muon info


  G4double energy = fEnergy;
  G4double  xcoord = fxcoord*cm;
  G4double  ycoord = fycoord*cm;
  G4double  xcosDirec = fxcosDirec;
  G4double  ycosDirec = fycosDirec;
  G4double  age = fage*s; 
  
   //make x directions negative for FASER coords

       G4ParticleDefinition* particleDefinition = G4ParticleTable::GetParticleTable()->FindParticle("mu-");
       fParticleGun->SetParticleDefinition(particleDefinition);
       fParticleGun->SetParticleEnergy(energy);
       fParticleGun->SetParticlePosition(G4ThreeVector(-xcoord, ycoord, -worldZHalfLength));
	    fParticleGun->SetParticleMomentumDirection(G4ThreeVector(-xcosDirec,ycosDirec,(1-(xcosDirec*xcosDirec)-(ycosDirec*ycosDirec))));
        fParticleGun->SetParticleTime(age); // set the particle time to the age
 

fParticleGun->GeneratePrimaryVertex(anEvent);


  

 

/*G4double eFin = 0.0;

for (int i=0;i<(muEnVec.size()-2);i++)    
 {
       if((muEnVec[i].first< randnum) && (randnum < muEnVec[i+2].first))
 { 
eFin = muEnVec[i+1].second;
} 

 }





//for energy distribution use eFin in stead of enSet

// G4double enSet =6800 *GeV; 
//for normal mode
 G4ParticleDefinition* particleDefinition 
    = G4ParticleTable::GetParticleTable()->FindParticle("mu-");

  fParticleGun->SetParticleDefinition(particleDefinition);
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1.));

  fParticleGun->SetParticlePosition(G4ThreeVector(0., 0., -worldZHalfLength));
 fParticleGun->SetParticleEnergy(eFin *GeV);
  fParticleGun->GeneratePrimaryVertex(anEvent);
*/


 }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
