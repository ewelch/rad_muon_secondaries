 #ifndef B2AnalysisManager_h
 #define B2AnalysisManager_h 1
 
 #include "globals.hh"
 
 #include "TROOT.h"
 #include "TFile.h"
 #include "TTree.h"
 #include <vector>
 
 class B2AnalysisManager
 {
 public:
   virtual ~B2AnalysisManager();
  
 
   ///method to call to create an instance of this class
   static B2AnalysisManager* getInstance();
 
   void Book(int);
   
   void AddEvent(std::vector<G4double>, std::vector<G4double>, std::vector<G4int>, std::vector<G4double>, std::vector<G4double>, std::vector<G4double>, G4double , G4double , G4double, G4double, G4double, G4double, G4double , G4double, G4double, G4double, G4double, G4double);
   
   void CloseFile();
 
 
 private:
   static const Int_t MAX_DET=100;
 
   ///private constructor in order to create a singleton
   B2AnalysisManager();
   static B2AnalysisManager* instance; 
   
   TFile* fFile;
   TTree* fTree;
 
   ///Store energy of the individual crystals
   Double_t fEnergy[MAX_DET];
   Double_t ffrad[MAX_DET];
   ///track length
   Double_t fTrackLength;
   Double_t fmuonEnergyB;
   Double_t fmuonpxB;
   Double_t fmuonpyB;
   Double_t fmuonpzB;
    Double_t fmuonEnergyA;
   Double_t fmuonpxA;
   Double_t fmuonpyA;
   Double_t fmuonpzA;
   Double_t fx;
   Double_t fy;
   Double_t fz;
   
   
  
   //pdg code

   Int_t fPDGCode[MAX_DET];
 
 
   Int_t fNsecondaries;
  
   Double_t fpx[MAX_DET];
   Double_t fpy[MAX_DET];
   Double_t fpz[MAX_DET];



 };
 
 #endif
 
